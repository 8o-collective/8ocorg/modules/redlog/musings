import React, { useState, useEffect, useRef } from "react";

import {
  TapeLoadContainer,
  TapeLoadText,
} from "assets/styles/TapeLoad.styles.jsx";

const TapeLoad = () => {
  const [loadText, setLoadText] = useState("BOOTING INTO REDLOG... ");

  const string = [
    "INITIALIZED.\n\n",
    "LOADING TAPES...\n",
    "TAPE DIRECTORY NOT FOUND.\n",
    "LOADING TRANSCRIPTIONS...\n",
    "TRANSCRIPTION DIRECTORY FOUND.\n",
    "PRESS ANY KEY TO CONTINUE...\n",
  ];

  const index = useRef(0);

  useEffect(() => {
    const tick = () => {
      setLoadText((prev) => prev + string[index.current]);
      index.current++;
    };
    if (index.current < string.length) {
      let addChar = setInterval(tick, 2000 * Math.random());
      return () => clearInterval(addChar);
    }
  }, [loadText]);

  return (
    <TapeLoadContainer>
      <TapeLoadText>{loadText}</TapeLoadText>
    </TapeLoadContainer>
  );
};

export default TapeLoad;
