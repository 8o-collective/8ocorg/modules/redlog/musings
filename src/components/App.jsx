import React from "react";

import { AppContainer, AppAccessLink } from "assets/styles/App.styles.jsx";

import accesstapes from "assets/text/accesstapes.jsx";

const App = () => (
  <AppContainer>
    <AppAccessLink href="/tapes">{accesstapes}</AppAccessLink>
  </AppContainer>
);

export default App;
