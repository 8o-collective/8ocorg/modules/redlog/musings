import React from "react";
import { useParams } from "react-router-dom";

import {
  TapeContainer,
  TapeTextContainer,
  TapeBackLink,
} from "assets/styles/Tape.styles.jsx";

const Tape = () => {
  const { id } = useParams();
  const text = require(`assets/text/${id}.jsx`).default;

  return (
    <TapeContainer>
      <TapeTextContainer dangerouslySetInnerHTML={{ __html: text }} />
      <TapeBackLink href="/tapes"> {"< Back"} </TapeBackLink>
    </TapeContainer>
  );
};

export default Tape;
