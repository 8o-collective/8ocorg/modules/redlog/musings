import React, { useState, useEffect } from "react";

import { TapesContainer, TapeLink } from "assets/styles/Tapes.styles.jsx";
import { TapeLoadText } from "assets/styles/TapeLoad.styles.jsx";

import TapeLoad from "components/TapeLoad.jsx";

const Tapes = () => {
  const [loaded, setLoaded] = useState(localStorage.getItem("loaded"));

  const handleKeyDown = () => {
    localStorage.setItem("loaded", true);
    setLoaded(true);
  };

  useEffect(() => {
    if (!loaded) {
      setTimeout(
        () =>
          ["mousedown", "keydown"].forEach((event) =>
            window.addEventListener(event, handleKeyDown)
          ),
        6000
      );

      return function cleanup() {
        ["click", "keydown"].forEach((event) =>
          window.addEventListener(event, handleKeyDown)
        );
      };
    }
  }, []);

  if (loaded) {
    return (
      <TapesContainer>
        <TapeLoadText>LOADING TRANSCRIPTIONS...</TapeLoadText>
        <TapeLink>
          {" "}
          <a style={{ color: "inherit" }} href="http://8oc.org">
            .
          </a>
        </TapeLink>
        <TapeLink>
          ├──{" "}
          <a style={{ color: "inherit" }} href="/tapes/1">
            {" "}
            TAPE 1 [TRANSCRIPTION]
          </a>
        </TapeLink>
        <TapeLink>
          ├──{" "}
          <a style={{ color: "inherit" }} href="/tapes/2">
            {" "}
            TAPE 2 [TRANSCRIPTION]
          </a>
        </TapeLink>
        <TapeLink>
          ├──{" "}
          <a style={{ color: "inherit" }} href="/tapes/3">
            {" "}
            TAPE 3 [TRANSCRIPTION]
          </a>
        </TapeLink>
        <TapeLink>
          ├──{" "}
          <a style={{ color: "inherit" }} href="/tapes/4">
            {" "}
            TAPE 4 [TRANSCRIPTION]
          </a>
        </TapeLink>
        <TapeLink>
          ├──{" "}
          <a style={{ color: "inherit" }} href="/tapes/5">
            {" "}
            TAPE 5 [TRANSCRIPTION]
          </a>
        </TapeLink>
      </TapesContainer>
    );
  } else {
    return <TapeLoad />;
  }
};

export default Tapes;
