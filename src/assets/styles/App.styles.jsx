import styled from "styled-components";

const AppContainer = styled.div`
  background-color: black;
  margin: 0px;
  position: fixed;
  width: 100%;
  height: 100%;
  top: 0px;
  left: 0px;
`;

const AppAccessLink = styled.a`
  display: block;
  position: absolute;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
  color: #00ff00;
  font-family: "IBM3270", monospace;
  font-size: 1vh;
  margin: auto;
  white-space: pre;
  text-decoration: none;
`;

export { AppContainer, AppAccessLink };
