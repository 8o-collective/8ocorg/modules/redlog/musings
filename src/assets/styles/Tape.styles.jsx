import styled from "styled-components";

const TapeContainer = styled.div`
  margin: 0px;
  position: absolute;
  width: 100%;
  height: 100%;
  top: 0px;
  left: 0px;

  background-color: black;
  color: red;
  font-family: "IBM3270", monospace;
  white-space: pre-line;
`;

const TapeTextContainer = styled.div`
  /* position: absolute;
    top: 50%;
    left: 50%;
    transform: translate(-50%, -50%); */
  margin: 10vw 10vw 0;

  line-height: 2em;
  font-size: 20px;
  font-family: "IBM3270", monospace;
  color: red;
  white-space: pre-line;
`;

const TapeBackLink = styled.a`
  display: inline-block;
  margin: 4vw 0 0 85vw;

  line-height: 2em;
  font-size: 20px;
  font-family: "IBM3270", monospace;
  color: #00ff00;
  white-space: pre-line;
`;

export { TapeContainer, TapeTextContainer, TapeBackLink };
