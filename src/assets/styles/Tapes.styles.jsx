import styled from "styled-components";

const TapesContainer = styled.div`
  background-color: black;
  margin: 0px;
  position: absolute;
  width: 100%;
  height: 100%;
  top: 0px;
  left: 0px;
  font-family: "IBM3270", monospace;
`;

const TapeLink = styled.div`
  margin: 0;
  text-decoration: none;
  display: block;
  font-size: min(5vh, 7vw);
  color: #00ff00;
`;

export { TapesContainer, TapeLink };
