import styled from "styled-components";

const TapeLoadContainer = styled.div`
  background-color: black;
  margin: 0px;
  position: absolute;
  width: 100%;
  height: 100%;
  top: 0px;
  left: 0px;
  cursor: pointer;
`;

const TapeLoadText = styled.div`
  font-family: "IBM3270", monospace;
  color: red;
  font-size: min(3vh, 5vw);
  white-space: pre;
`;

export { TapeLoadContainer, TapeLoadText };
