export default `
  <b>Interviewer:</b> So, what is 8oC?

  <b>The Operator:</b> You know, when Feynman was once asked about the nature of the magnetic field and 'why' two magnets repel, he went into one of those long physics professor rants about how when you get into the specifics of the universe, eventually you just have to assume axioms somewhere or else you'll get caught in a perpetual loop of questioning that ends with the fact that human knowledge is limited. 8oC is just one of those things you have to assume is <b>implicitly true.</b>
`;
