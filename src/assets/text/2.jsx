export default `
  <b>Interviewer:</b> If you had to define it, what is 8oC?

  <b>Guy:</b> You know the feeling you get when you're in a city walking alone at night and you hear a foot scraping behind you? Keep that feeling in mind. You know when you see a very cute animal, let's say a puppy, let's say a cute little bug? Also keep that in mind. Ok, next feeling: You get woken up by birds chirping outside your window -- keep that in mind. You're a little child lost in the supermarket. Mind; again. You read a story that gets just too many details of your personal life right to be ignored. You still remember the dog? The one I asked you to keep in mind? Really? You forgot about it? Yeah, the feeling you feel right now, also keep it in mind. Don't forget the dog. Now mix them all together, maybe add a little extra dog in, might be nicer that way. That's 8oC.
`;
