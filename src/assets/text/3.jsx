export default `
  <b>Interviewer:</b> To cap this interview off: what is 8oC?

  <b>Man beyond the Void:</b> "What is 8oC?" Really? You're gonna ask me that? I thought it was pretty clear, but I guess not everyone has a functioning set of eyes. I'll tell you what 8oC is: it's too much for you to handle. So don't worry your pretty little head about it, and let the grownups do their thing. I'll let you in on a secret: it doesn't matter whether or not you "get" what 8oC is. In fact, nothing could matter less! Buddy, truth is, if you were truly capable of having any impact on 8oC, even if only via the little butterfly effect hurricanes your sad, pathetic sneezes might help start, then you'd have figured out what it was by now...
`;
