export default `
  <b>Interviewer:</b> What-- what is 8oC?

  <b>Gemini of a Clay Machine:</b> Of course, 8oC, one moment please, I'm terribly busy, hold this, where were we, ah yes, 8oC, well, of course it's a-- one moment, yes, that's good, give that back please, back up just a bit-- there we are, one foot over the fence, swing the other, that's perfect, easy on the landing, bad for your knees you know, where were we again, that's right, 8oC -- armed guard, eyes up -- it's really something of a, sorry, ease up on the hilt there, there we go, something like a -- pull this pin there -- collective for -- and toss, ears plugged for a moment -- [INAUDIBLE]. Whew, glad I got that out. You may want to run.
`;
