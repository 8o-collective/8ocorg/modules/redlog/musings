import React from "react";
import { Routes, Route } from "react-router-dom";

import App from "components/App.jsx";
import TapeLoad from "components/TapeLoad.jsx";
import Tapes from "components/Tapes.jsx";
import Tape from "components/Tape.jsx";

const NotFound = () => {
  window.location.replace("http://invite.8oc.org");
};

const routes = (
  <Routes>
    <Route path="*" element={<NotFound />} />
    <Route exact path="/" element={<App />} />
    <Route path="/tapeload" element={<TapeLoad />} />
    <Route exact path="/tapes" element={<Tapes />} />
    <Route path="/tapes/:id" element={<Tape />} />
  </Routes>
);

export { routes };
